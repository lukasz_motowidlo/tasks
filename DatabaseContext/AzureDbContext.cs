﻿using Microsoft.EntityFrameworkCore;
using tasks.Models;

namespace tasks.DatabaseContext
{
    public class AzureDbContext : DbContext
    {
        public AzureDbContext(DbContextOptions<AzureDbContext> options) : base(options)
        {

        }

        protected AzureDbContext()
        {

        }

        public DbSet<Person> People { get; set; }
    }
}